module.exports = app => {
  const users = require("../controllers/users.controller.js");

  var router = require("express").Router();

  // Retrieve all Users
  router.get("/", users.findAll);

  // Create  Users
  router.post("/", users.create);


  app.use('/api/users', router);
};
