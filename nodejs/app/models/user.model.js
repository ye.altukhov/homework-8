module.exports = (sequelize, Sequelize) => {
  const User = sequelize.define("user", {
    name: {
      type: Sequelize.STRING
    },
    dob: {
      type: Sequelize.DATE
    }
  });

  return User;
};
