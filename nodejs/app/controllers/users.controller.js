const db = require("../models");
const User = db.users;

// Create and Save a new User
exports.create = (req, res) => {

 for(let i = 0; i <= 40000000; i++) {
   // Create a User
   const user = {
     name: (Math.random() + 1).toString(36).substring(7),
     date: new Date(),
   };

   // Save User in the database
   User.create(user)
       .then(data => {
         res.send(data);
       })
       .catch(err => {
         res.status(500).send({
           message:
               err.message || "Some error occurred while creating the User."
         });
       });
 }
};

// Retrieve all Users from the database.
exports.findAll = (req, res) => {
  User.findAll()
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving users."
      });
    });
};

