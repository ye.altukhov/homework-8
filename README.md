Run docker-compose up -d to launch containers.

Go to mysql console mysql -u root -p 123456

CREATE TABLE IF NOT EXISTS users ( id int NOT NULL AUTO_INCREMENT PRIMARY KEY, name varchar(255),  dob date, createdAt date, updatedAt date  );

Make POST http://localhost:3000/api/users - adding 40M users

No index:
innodb_flush_log_at_trx_commit=1 33103 sec
innodb_flush_log_at_trx_commit=2 36285 sec
innodb_flush_log_at_trx_commit=0 34924 sec

select dob FROM users WHERE DATE(dob) > '2021-22-09' LIMIT 8000;
No index: 8000 rows in 0.83 sec
Btree index 8000 rows in 11.49 sec
Hash index 8000 rows in 19.02 sec